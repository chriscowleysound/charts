# Deadman Http Daemon

Default Deadman Switch implementation (WatchDog alert) for Prometheus Operator setups sends a
`POST` request continually, actually to /dev/null .

The objective of this project is to implement the other side of
[Deadman's Switch](https://en.wikipedia.org/wiki/Dead_man%27s_switch):

* receive `POST` requests from Prometheus continually,
* for every `POST`, update last seen ping,
* and if ping is not updated for a period of time, send an alert to a receiver
  bypassing AlertManager

It's important to receive `GET` requests in a regular basis, because that's the
mecanism used to check last seen ping and transition to a different state if
needed.

This server handles 2 type of HTTP requests:

* `GET` requests for healtchecks  on path `/httprobe`
* `POST` requests for alerts/resolves

For more details, take a look on all Deadman related modules:

*  [deadman-http-daemon](https://gitlab.com/rsicart/deadman-http-daemon)
*  [deadman-state-machine](https://gitlab.com/rsicart/deadman-state-machine)


## Configuration

The following table lists some configurable parameters of this chart and their default values.

Parameter | Description | Default
--- | --- | ---
`affinity` | node/pod affinities (requires Kubernetes >=1.6) | `{}`
`deadman.debug` | enable verbose logging | `false`
`deadman.port` | port number where deadman http daemon listens | `8000`
`deadman.timeout` | number of seconds to wait before sending an alert to receivers | `300`
`image.pullPolicy` | container image pull policy | `Always`
`image.repository` | container image repository | `registry.gitlab.com/rsicart/deadman-http-daemon`
`image.tag` | container image tag | `v0.1.5`
`ingress.annotations` | configure ingress annotations | `{}`
`ingress.enabled` | create an ingress to expose the service | `false`
`ingress.hosts` | list of hosts | `[ "deadman-example.local" ]`
`ingress.path` | configure ingress path | `/`
`ingress.tls` | list of secrets to use with tls | `[]`
`nodeSelector` | node labels for pod assignment | `{}`
`replicaCount` | desired number of controller pods | `1`
`resources` | pod resource requests & limits | `{}`
`service.port` | service port number | `80`
`service.type` | service type to create | `ClusterIP`
`settings` | multiline string to pass python settings to deadman http daemon | `See values.yaml`
`tolerations` | node taints to tolerate (requires Kubernetes >=1.6) | `[]`
